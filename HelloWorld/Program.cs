﻿using System;
using Aui.Location;

namespace HelloWorld
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			User a = new User();
			a.age = 10;
			a.name = "Aui";
			a.show();

			//////
			Address addr = new Address();
			addr.homeNumber = "test";
			Console.WriteLine(addr.getHomeNumber());

			Product prod = new Product();

			prod.setName("ผักคะน้า");
			prod.setPrice(500);
            prod.addToCart(prod);

			prod.setName("กระดาษ");
			prod.setPrice(200);
			prod.addToCart(prod);

			prod.getRecommendation();
			prod.getCategory();
			prod.getReducedPrice("high");

			prod.addToCart(prod);

			Console.WriteLine("name:"+prod.getName());
			Console.WriteLine("price:"+prod.getPrice());
			Console.WriteLine("getCatagory:" + prod.getCategory());
			Console.WriteLine("getRecommendation"+prod.getRecommendation());

			String levels= "high";
			Console.WriteLine("getReducedPrice:"+prod.getReducedPrice(levels));

			prod.printAllProductInCart();
		}
	}
}
